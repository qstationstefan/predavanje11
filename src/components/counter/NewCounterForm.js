import { useState, useContext } from "react";
import StatsContext from "../../context/stats_context";

const NewCounterForm = (props) => {
    const [incrementStep, setIncrementStep] = useState(1);
    const statsContext = useContext(StatsContext);

    const incrementChangeHandler = (e) => {
        setIncrementStep(+e.target.value);
    }

    const submitHandler = (e) => {
        e.preventDefault();
        props.onAddCounterIncrement(incrementStep);
        statsContext.setCountersAmount(prevState => prevState + 1);
    }

    return (
        <form onSubmit={submitHandler}>
            <input onChange={incrementChangeHandler} value={incrementStep} />
            <button type="submit">Add new counter</button>
        </form>
    )
}

export default NewCounterForm;