import { useContext } from "react";
import StatsContext from "../../context/stats_context";

const CounterStats = () => {
    const context = useContext(StatsContext);
   
    return (
        <div>Counters amount: {context.countersAmount}</div>
    )
}

export default CounterStats;