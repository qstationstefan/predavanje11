import { useState, useEffect, useRef, useContext } from "react";
import StatsContext from "../../context/stats_context";
import TimerLap from "./TimerLap";

const Timer = () => {
    const [timerValue, setTimerValue] = useState(0);
    const [isRunning, setIsRunning] = useState(false);
    const [laps, setLaps] = useState([]);
    const lasLapStart = useRef(0);
    const minIndex = useRef(0);
    const maxIndex = useRef(0);
    const idAutoIncrement = useRef(0);
    const context = useContext(StatsContext);

    useEffect(() => {
        let id;
        if (isRunning) {
            id = setInterval(() => {
                setTimerValue(prevState => prevState + 1);
            }, 10);
        }

        return () => {
            if (id) {
                clearInterval(id);
            }
        }
    }, [isRunning]);

    const toggleTimerHandler = () => {
        setIsRunning(prevState => !prevState);
    }

    const addLapHandler = () => {
        idAutoIncrement.current++;
        const lapDuration = timerValue - lasLapStart.current;
        lasLapStart.current = timerValue;
        if (laps.length >= 1) {
            if (lapDuration < laps[minIndex.current].duration) {
                minIndex.current = laps.length;
            }
            if (lapDuration > laps[maxIndex.current].duration) {
                maxIndex.current = laps.length;
            }
        }
        setLaps(prevState => [...prevState, { time: timerValue, duration: lapDuration, id: idAutoIncrement.current }])
        // INCREMENT OVDJE
        context.incrementTimerLaps();
    }


    return (
        <div>
            <p>{`Time: ${(timerValue / 100)}s`}</p>
            <button onClick={toggleTimerHandler}>Start/Stop timer</button>
            <button disabled={!isRunning} onClick={addLapHandler}>Lap</button>
            {laps.map((lap, index) => <TimerLap key={lap.id} time={lap.time} duration={lap.duration} isMax={laps.length > 1 && maxIndex.current === index} isMin={laps.length > 1 && minIndex.current === index} />)}
        </div>
    )
}

export default Timer;