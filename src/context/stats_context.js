import React, { useState } from "react";

const StatsContext = React.createContext(
    {
        countersAmount: 0,
        timersAmount: 0,
        timersLaps: 0,
        setCountersAmount: () => { },
        incrementTimerLaps: () => { }
    });

export const StatsContextProvider = (props) => {
    const [countersAmount, setCountersAmount] = useState(0);
    const [timersAmount, setTimersAmount] = useState(0);
    const [timersLaps, setTimersLaps] = useState(0);

    const incrementTimerLaps = () => {
        setTimersLaps(prevState => prevState + 1);
    }


    return (
        <>
            <StatsContext.Provider value={
                {
                    countersAmount: countersAmount,
                    timersAmount: timersAmount,
                    timersLaps: timersLaps,
                    setCountersAmount,
                    incrementTimerLaps
                }}>
                {props.children}
            </StatsContext.Provider>
        </>
    )
}

export default StatsContext;