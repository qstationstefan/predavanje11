import React, { useState, useReducer } from 'react';
import NewCounterForm from "./components/counter/NewCounterForm";
import CounterList from "./components/counter/CounterList";
import TimerList from "./components/timer/TimerList";
import MainStats from './components/stats/MainStats';

const INITIAL_STATE = {
  username: "",
  password: "",
  isLoading: false,
  error: "",
  isLoggedIn: false
}

const LOGIN_START = "login_start";
const SUCCESS = "success";
const ERROR = "error";
const USERNAME = "username";
const PASSWORD = "password";

const loginReducer = (state, action) => {
  switch (action.type) {
    case USERNAME:
      return {
        ...state,
        username: action.payload
      }
    case PASSWORD:
      return {
        ...state,
        password: action.payload
      }
    case LOGIN_START:
      return {
        ...state,
        error: "",
        isLoading: true
      }
    case SUCCESS:
      return {
        ...state,
        isLoggedIn: true
      }
    case ERROR:
      return {
        ...state,
        error: 'Incorrect username or password!',
        isLoading: false,
        username: "",
        password: ""
      }
    default:
      return state;
  }
}

const App = () => {
  const [loginState, loginDispatch] = useReducer(loginReducer, INITIAL_STATE);
  const [counterIncrements, setCounterIncrements] = useState([]);

  const login = async () => {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        if (loginState.username === 'a' && loginState.password === 'a') {
          resolve();
        } else {
          reject();
        }
      }, 1000);
    });
  }

  const onSubmit = async (e) => {
    e.preventDefault();
    loginDispatch({ type: LOGIN_START })
    try {
      await login();
      loginDispatch({ type: SUCCESS });
    } catch (err) {
      loginDispatch({ type: ERROR });
    }
  };


  const addCounterValueIncrement = (incrementStep) => {
    setCounterIncrements(prevState => [...prevState, incrementStep]);
  }

  return (
    <div className='App'>
      <div className='login-container'>
        {loginState.isLoggedIn ? (
          <div>
            <MainStats />
            <div>
              <h1>Counters:</h1>
              <NewCounterForm onAddCounterIncrement={addCounterValueIncrement} />
              <CounterList counterIncrements={counterIncrements} />
            </div>
            <TimerList />
          </div>
        ) : (
          <form className='form' onSubmit={onSubmit}>
            {loginState.error && <p className='error'>{loginState.error}</p>}
            <p>Please Login!</p>
            <input
              type='text'
              placeholder='username'
              value={loginState.username}
              onChange={(e) => loginDispatch({ type: USERNAME, payload: e.target.value })}
            />
            <input
              type='password'
              placeholder='password'
              autoComplete='new-password'
              value={loginState.password}
              onChange={(e) => loginDispatch({ type: PASSWORD, payload: e.target.value })}
            />
            <button className='submit' type='submit' disabled={loginState.isLoading}>
              {loginState.isLoading ? 'Logging in...' : 'Log In'}
            </button>
          </form>
        )}
      </div>
    </div>
  );
}

export default App;